import React from 'react';
import "./App.css";

export default class CellModal extends React.Component {
    render (){
        return (
        <div className="modal">
            <div className="modal-content">{this.props.prevMonth}</div>
        </div>
        );
    };
};
