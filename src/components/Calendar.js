import React from 'react';
import "./App.css";
import Cell from './Cell.js';

export default class Calendar extends React.Component {
    state = {
        currentDate: new Date(),
        selectedDate: new Date()
    };

    renderHeader() {
        return (
            <div className="header row">
                <div className="col col-start">
                    <div className="icon" onClick={this.prevMonth}>
                        chevron_left
                    </div>
                </div>

                <div className="col col-center">
                    <span>
                        {this.renderMonthYear()}
                    </span>
                </div>
                <div className="col col-end" onClick={this.nextMonth}>
                    <div className="icon">chevron_right</div>
                </div>
            </div>
        );
    };

    renderMonthYear() {
        const months = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December'];

        return months[this.state.currentDate.getMonth()] + ' ' + this.state.currentDate.getFullYear();
    };

    renderDays() {
        const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

        for (let i = 0; i < 7; i++) {
            days[i] =
                <div className="col col-center" key={i}>
                    {days[i]}
                </div>;
        }
        return <div className="days row">{days}</div>;
    };

    renderCells() {
        let cells = [];
        let row = [];
        let monthStart = new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth(), 1);
        let prevMonth = new Date(this.state.currentDate.getFullYear(), this.state.currentDate.getMonth(),
            -(monthStart.getDay() - 1));


        for (let i = 0; i < 6; i++) {
            for (let j = 0; j < 7; j++) {
                row.push(<Cell prevMonth={prevMonth.getDate()} key={j} />);
                prevMonth.setDate(prevMonth.getDate() + 1);
            }
            cells.push(<div className=" row" key={i}>{row}</div>);
            row = [];
        }
        return <div className="body">{cells}</div>;
    }

    onDateClick = day => { };

    nextMonth = () => {
        this.setState(prevState => ({
            currentDate: new Date(prevState.currentDate.getFullYear(), prevState.currentDate.getMonth() + 1)
        }));
    };

    prevMonth = () => {
        this.setState(prevState => ({
            currentDate: new Date(prevState.currentDate.getFullYear(), prevState.currentDate.getMonth() - 1)
        }));
    };

    render() {
        return (
            <div className="calendar">
                {this.renderHeader()}
                {this.renderDays()}
                {this.renderCells()}
            </div>
        );
    }
}