import React from 'react';
import "./App.css";
import CellModal from "./CellModal.js";
import onClickOutside from "react-onclickoutside";

class Cell extends React.Component {
    state = {
        selected: false
    };
    toggleSelected = (e) => {
        e.preventDefault();
        this.setState((prevState) => ({selected: !prevState.selected}),
            () => { console.log(this.state.selected);});
    };

    handleClickOutside = () => {
        if (this.state.selected) {
            this.setState({selected: false},
                () => { console.log(this.state.selected);});
        }
    };

    render() {
        return (
            <div className="col cell" onClick={this.toggleSelected}>
            {this.state.selected && <CellModal prevMonth={this.props.prevMonth}/>}
            <div>{this.props.prevMonth}</div>
            </div>
        );
    };
}

export default onClickOutside(Cell);