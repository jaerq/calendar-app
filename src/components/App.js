import React from 'react';
import Calendar from './Calendar';

export default class App extends React.Component {
    render(){
        return (
            <div className="App">
                <header>
                    <div id="logo">
                        <span className="icon">date_range</span>
                        <span id="logo-text">
                            gruv<b>cal</b>
                        </span>
                    </div>
                </header>
            <main>
                <Calendar />
            </main>
            </div>

        );
    }
}